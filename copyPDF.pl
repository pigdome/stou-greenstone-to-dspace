use File::Find::Rule;
use strict;
use warnings;
use File::Copy;

my $srcdir = $ARGV[0];  #pdf-mini
my $destdir = $ARGV[1]; #pdf
my @subdir_dest = File::Find::Rule->directory->in($destdir);
my @subdir_src = File::Find::Rule->directory->in($srcdir);
	
    foreach my $subdir (@subdir_src) 
    {
   		opendir(DIR, ( $subdir ) ) or die $!;
		while (my $file = readdir(DIR)) 
		{
            if ( index( $file , '.pdf') >= 0 ) 
            {
            	my $subdir_temp = $subdir;
                $subdir_temp =~ s/$srcdir/$destdir/g;
            	copy( $subdir . '/' . $file , $subdir_temp . '/' . $file ) or die "Copy failed: $!";
            	print "copy $file successfully \n";
            }
		}
		closedir(DIR);
    }
