import java.io.*;
import java.util.*;

public class ConvertHtmlToMetImg
{
	public static String baseDir = "/D:/gsdl/collect/nonthaburi/import-test-img";

	public static void main(String[] args) 
	{
       String[] directories = ConvertHtmlToMetImg.listDir(baseDir);
		
		for( String subDir : directories )
		{
			String[] listfile = ConvertHtmlToMetImg.listFile(baseDir+"/"+subDir);
            ConvertHtmlToMetImg.convertMODS(subDir,listfile);
		}
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}
    
    public static void convertMODS(String subDir,String[] listfile)
    {
    	BufferedWriter writer = null;
    	BufferedReader br = null;
        BufferedWriter writerMODS = null;
        BufferedWriter writerDcTerms = null;
        BufferedWriter test = null;

        String hmtlname = "";
        String line;
        String temp="";
        String title = "";

        ArrayList<String> arrData = new ArrayList<String>();
    	for( String filename : listfile )
    		if( filename.indexOf(".html") >= 0 && filename.indexOf(".bak") < 0 )
                  hmtlname = filename;
      
    	try
    	{
            br = new BufferedReader(new InputStreamReader(
            	 new FileInputStream(baseDir+"/"+subDir+"/"+hmtlname),"UTF-16LE" ));
    	    writer = new BufferedWriter(new OutputStreamWriter(
    	    	new FileOutputStream(baseDir+"/"+subDir+"/dublin_core.xml"), "UTF8"));
    	    writer.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>\n");
    	    writer.write("<dublin_core schema=\"dc\">\n");

            writerMODS = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/metadata_mods.xml"), "UTF8"));
            writerMODS.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
            writerMODS.write("<dublin_core schema=\"mods\">");

            writerDcTerms = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/metadata_dcterms.xml"), "UTF8"));
            writerDcTerms.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
            writerDcTerms.write("<dublin_core schema=\"dcterms\">");

            test = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/log.txt"), "UTF8"));
    	    


            while((line=br.readLine())!= null)
            {   
            	if( line.indexOf("cccccc") >= 0 )
            	{
                    while((line=br.readLine())!= null)
            		{
            			if( line.indexOf("<TR") >= 0 )
            			{
            				temp = line;
            				while((line=br.readLine())!= null)
            				{
                                 temp += subSpaceInStr(line).trim();
                                 if( line.indexOf("</TR>") >= 0 )
                                 	break;
            				}
                            //test.write(temp);
            				arrData.add( ConvertHtmlToMetImg.subString(temp) );
            			}
            		}
            	}   
            }
            
            // write log file
            for( String str : arrData )
                test.write(str+"\n");

            title = ConvertHtmlToMetImg.choose(arrData,writer,writerMODS,writerDcTerms);
            ConvertHtmlToMetImg.createContents(title,listfile,subDir);

            writer.write("</dublin_core>");
            writer.close();
            writerMODS.write("</dublin_core>");
            writerMODS.close();
            writerDcTerms.write("</dublin_core>");
            writerDcTerms.close();
            test.close();
    	}
    	catch( FileNotFoundException e ){e.printStackTrace();}
		catch (IOException e) {
			e.printStackTrace();
		}
         
    }

    public static String subString(String elem)
    {
    	String line = elem;
        while( line.indexOf("<") >= 0 )
        {
            String unused = line.substring(line.indexOf("<"),line.indexOf(">")+1);
            line = line.replace(unused,"").trim();
        }
        return line;
    }

    public static String choose(ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS,BufferedWriter writerDcTerms)throws IOException
    {
         String title = "";
         int i = 0;
         for( String line : arrData )
         {
             if( line.indexOf("Related Item :Constituent") >= 0 )
                line = line.substring( 0,line.indexOf("(") );
               
            
              switch( line.trim() )
              {
                 case "TitleInformation"          : title = getInfoInTitle(i,arrData,writer,writerMODS);break;
                 case "Language"                  : getInfoInLanguage(i,arrData,writer,writerMODS);break;
                 case "Subject"                   : getInfoInSubject(i,arrData,writer,writerMODS);break;
                 case "Related Item :Host (In)"   : getInfoInRHost(i,arrData,writer,writerMODS);break;
                 case "Related Item :series"      : getInfoInRSeries(i,arrData,writer,writerMODS);break;
                 case "PhysicalDescription"       : getInfoInPhysical(i,arrData,writer,writerMODS);break;
                 case "Related Item :otherFormat" : getInfoInRo(i,arrData,writer,writerMODS,writerDcTerms);break;
                 case "Related Item :Constituent" : getInfoInCo(i,arrData,writer,writerMODS);break;
                 
              }
              i++;
         }
         return title;
    }
    
    private static String getInfoInTitle(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String namepart = "";
        String title = "";
        String line = arrData.get(j);
        while( line.indexOf("Language") < 0 && j < arrData.size())
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("TitleInformation") < 0 && line.split(":").length > 1 && j < 5)
            {
              title =  line.split(":")[1].trim();
              writer.write("<dcvalue element=\"title\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");
            }

            if( line.indexOf("Name Part : given") >= 0 )
              namepart = line.split(":")[2].trim();

            if( line.indexOf("Date Created (marc)") >= 0 )
              writer.write("<dcvalue element=\"date\" qualifier=\"created\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Type ofResource") >= 0 )
              writer.write("<dcvalue element=\"type\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Genre") >= 0 )
              writerMODS.write("<dcvalue element=\"genre\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");
            
            // next cursor          
            line = arrData.get(j++);
        }
        return title;
    }

    private static void getInfoInLanguage(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("PhysicalDescription") < 0 && j < 20)
        {                    
            if( line.indexOf("Language") >= 0 && line.indexOf("Term") >= 0 )
            {
              writer.write("<dcvalue element=\"language\" qualifier=\"iso\">"+line.split(":")[2].trim()+"</dcvalue>\n");
            }

            // next cursor          
            line = arrData.get(j++);
        }
    }

    private static void getInfoInPhysical(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("Subject") < 0 && j < arrData.size())
        {
            if( line.indexOf("Extent") >= 0 )
              writer.write("<dcvalue element=\"format\" qualifier=\"extent\">"+line.replace("Extent :","").trim()+"</dcvalue>\n");

            if( line.indexOf("digitalOrgin") >= 0 || line.indexOf("digitalOrigin") >= 0)
              writerMODS.write("<dcvalue element=\"physicalDescription\" qualifier=\"digitalOrigin\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Form (marcform)") >= 0 )
              writerMODS.write("<dcvalue element=\"physicalDescription\" qualifier=\"form\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("reformattingQuality") >= 0 )
              writerMODS.write("<dcvalue element=\"physicalDescription\" qualifier=\"reformattingQuality\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Internet Media Type") >= 0 )
              writer.write("<dcvalue element=\"format\" qualifier=\"mimetype\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Note :general") >= 0 )
              writer.write("<dcvalue element=\"description\" qualifier=\"none\">"+line.split(":")[2].trim()+"</dcvalue>\n");
             
            // next cursor           
            line = arrData.get(j++);
        }
    }

    private static void getInfoInSubject(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String line = arrData.get(j++);
        while( j-i < 5 )
        {
            if( line.indexOf("Geographic") >= 0 && line.split(":").length > 1)
            {
              writer.write("<dcvalue element=\"subject\" qualifier=\"other\">"+line.split(":")[1].trim()+"</dcvalue>\n");
              break;
            }

            if( line.indexOf("Name Part") >= 0 )
            {
              if( line.split(":").length == 2 )
                writer.write("<dcvalue element=\"subject\" qualifier=\"other\">"+line.split(":")[1].trim()+"</dcvalue>\n");
              if( line.split(":").length == 3 )
                writer.write("<dcvalue element=\"subject\" qualifier=\"other\">"+line.split(":")[2].trim()+"</dcvalue>\n");
              break;
            }

            if( line.indexOf("Topic") >= 0 && line.split(":").length > 1)
            {
              writer.write("<dcvalue element=\"subject\" qualifier=\"other\">"+line.split(":")[1].trim()+"</dcvalue>\n");
              break;
            }
             
            // next cursor          
            line = arrData.get(j++);
        }
    }                          

    
    private static void getInfoInRHost(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        String title = "";
        String partname = "";
        String genre = "";
        String start = "";
        String end = "";
        String lcc = "";
        String role = "";
        ArrayList<String> personalname = new ArrayList<String>();
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("Related Item :series") < 0 && j < arrData.size())
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("Title Information") < 0)
              title = line.split(":")[1].trim();

            if( line.indexOf("partName") >= 0 )
              partname = line.split(":")[1].trim();

            if( line.indexOf("Name Part : given") >= 0 )
              personalname.add(line.split(":")[2].trim());

            if( line.indexOf("Genre") >= 0 )
              genre = line.split(":")[1].trim();

            if( line.indexOf("Start") >= 0 )
              start = line.replace("Start","").trim();

            if( line.indexOf("End") >= 0 )
              end = line.replace("End","").trim();
            
            if( line.indexOf("Classification (LCC)") >= 0 )
              lcc = line.split(":")[1].trim();

            if( line.indexOf("Role Term : Text") >= 0 )
              role = line.replace("Role Term : Text","").trim();              
            
            if( line.indexOf("Date Issued (marc)") >= 0 )
              writer.write("<dcvalue element=\"date\" qualifier=\"issued\">"+line.split(":")[1].trim()+"</dcvalue>\n");
             
            // next cursor          
            line = arrData.get(j++);
        }
        String personal = "";
        for( String person : personalname )
        {
           if(role.indexOf("editor") >= 0)
             personal += person+", "+role+"; ";
           else
             personal += person+"; ";
        }
        if( personal.indexOf(";") >= 0 )
        personal = personal.substring(0,personal.lastIndexOf(";"));

        if( lcc.compareTo("") != 0 )
            genre = genre+"("+lcc+")";
        String output = title +" : "+ partname + " / " + personal + " ["+genre+" , "+start+"-"+end+" ]";
        writer.write("<dcvalue element=\"relation\" qualifier=\"ispartof\">"+output+"</dcvalue>\n");

    }

    private static void getInfoInRSeries(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        String title= "";
        String partname = "";
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("Related Item :otherFormat") < 0 && j < arrData.size())
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("Title Information") < 0 && line.split(":").length == 2 )
              title = line.split(":")[1].trim();

            if( line.indexOf("partmane") >= 0 || line.indexOf("partname") >= 0)
              partname = line.split(":")[1].trim();

            // next cursor          
            line = arrData.get(j++);
        }
        if(title.indexOf(":") >= 0)
            title.substring(0,title.lastIndexOf(":"));
        if( partname.compareTo("") != 0  )
          writer.write("<dcvalue element=\"relation\" qualifier=\"ispartofseries\">"+title+" : "+partname+"</dcvalue>\n");
        else
          writer.write("<dcvalue element=\"relation\" qualifier=\"ispartofseries\">"+title+"</dcvalue>\n");

    }

    private static void getInfoInRo(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS,BufferedWriter writerDcTerms)throws IOException
    {
        String title="";
        String form = "";
        String extent = "";
        String note ="";
        int j = i;
        String line = arrData.get(j);
        while( j < arrData.size() )
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("Title Information") < 0 && line.split(":").length == 2)
              title = line.split(":")[1].trim();

            if( line.indexOf("Form (marcform)") >= 0 )
              form = line.split(":")[1].trim();
            
            if( line.indexOf("Extent") >= 0 )
              extent = line.replace("Extent :","").trim();

             if( line.indexOf("Note : content") >= 0 )
              note = line.split(":")[2].trim();
  
            if( line.indexOf("Physical Location") >= 0 )
              writerMODS.write("<dcvalue element=\"location\" qualifier=\"physicalLocation\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Access Condition") >= 0 )
              writerDcTerms.write("<dcvalue element=\"accessRights\" qualifier=\"none\">"+line.split(":")[2].trim()+"</dcvalue>\n");
            
              
            // next cursor  Access Condition        
            line = arrData.get(j++);
        }
        if( note.compareTo("") == 0 )
          writer.write("<dcvalue element=\"relation\" qualifier=\"hasformat\">"+form+" : "+title+" ; "+extent+"</dcvalue>\n");
        else
          writer.write("<dcvalue element=\"relation\" qualifier=\"hasformat\">"+form+" : "+title+" ; "+extent+"("+note+")"+"</dcvalue>\n");
    }

    private static void getInfoInCo(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        String title="";
        String code = "";
        int j = i;
        String line = arrData.get(j);
        while( j-i < 6 )//
        {
            if( line.indexOf("Related Item :Constituent") >= 0 )
            {
              code = line.replace("Related Item :Constituent","").trim();
              code = code.substring(code.indexOf("(")+1 , code.indexOf(")")).trim();
            }

            if( line.indexOf("Title") >= 0 && line.indexOf("Title Information") < 0 && line.split(":").length == 2)
              title = line.split(":")[1].trim();
             
            // next cursor  Access Condition        
            line = arrData.get(j++);
        }
        
        writer.write("<dcvalue element=\"relation\" qualifier=\"haspart\">"+title+" ["+code+"]</dcvalue>\n");
        
    }                                                                   
                  

    private static String subSpaceInStr(String str)
    {
         String[] split = str.split(" ");
         String out = "";
         for( String word : split )
         	if( word.compareTo("") != 0 )
         		out += word+" ";
         return out.replace("\n","");
    }

    private static void createContents(String title,String[] listfile,String subDir)
    {
        BufferedWriter writer = null;
        try
        {
            writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/contents"), "UTF8"));
            String pdfname = "";
            String htmlname ="";
            for( String filename : listfile )
            {
                if(filename.indexOf(".pdf") >= 0)
                    pdfname = filename;
                if( filename.indexOf(".html") >= 0 && filename.indexOf(".bak") < 0 )
                    htmlname = filename;
            }
            if( pdfname.compareTo("") == 0 )
            {
                // img dir
                ArrayList<String> imgname = new ArrayList<String>();
                for( String filename : listfile )
                {
                    if(filename.indexOf(".jpg") >= 0 && filename.indexOf("_A") >= 0)
                        imgname.add(filename);
                }
                for( String img : imgname )
                {
                    writer.write(img+"\tbundle:ORIGINAL\tdescription:"+title.trim()+"\n");
                }
            }
            else
            {
                // pdf dir
                writer.write(pdfname+"\tbundle:ORIGINAL\tdescription:"+title.trim()+"\n");
            }
            if( htmlname.compareTo("") != 0 )
                writer.write(htmlname+"\tbundle:ORIGINAL\tdescription:source metadata\n");

            writer.close();
        }
        catch( FileNotFoundException e ){}
        catch (IOException e) {
            e.printStackTrace();
        }
        

    }
	

}