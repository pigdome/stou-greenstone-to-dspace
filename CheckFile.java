import java.io.*;
import java.io.File;
import java.util.*;
import java.nio.channels.FileChannel;
public class CheckFile
{
	public static String baseDir = "/C:/gst/collect/stou/import";
	public static String oldDir = "/C:/gst/collect/stou/import-copy";
	public static BufferedWriter writeLog = null;
	public static int countMissingFile = 0;
	public static void main(String[] args)throws IOException
	{
		
		try
		{
			writeLog = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(baseDir+"\\logfile.txt"), "UTF8"));
		}
		catch( FileNotFoundException e ){}
		catch(IOException e){}

		String[] directories = CheckFile.listDir(baseDir);
		
		for( String subDir : directories )
		{
			copyImg(subDir);
			String[] listfile = CheckFile.listFile(baseDir+"/"+subDir);
			CheckFile.checkFile(baseDir+"/"+subDir+"/contents",baseDir+"/"+subDir,listfile);
		}
		writeLog.close();
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	public static void checkFile(String contentsDir,String dir,String[] arr_pdf)
	{
		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(contentsDir), "UTF8"));
			String line;
			ArrayList<String> arrTemp = new ArrayList<String>();
				
		    while( (line=br.readLine())!= null )
		    {
	             if( line.indexOf(".pdf") >= 0 || line.indexOf(".jpg") >= 0 )
	             {
	             	String[] split = line.split("\t");
				    if( ! CheckFile.search( split[0],arr_pdf ) )
	                  arrTemp.add( CheckFile.createMissingFile(line,dir) );
	                else
	             	  arrTemp.add(line);
	             }
	             else
	             	arrTemp.add(line);
	             
	        }
	        CheckFile.createContens(dir,arrTemp);
		}
		catch( FileNotFoundException e ){}
		catch(IOException e){}

		
	}

	public static boolean search(String str,String[] arr_pdf)throws IOException
	{
        for( String filename : arr_pdf )
        {
        	if( str.toLowerCase().compareTo( filename.toLowerCase() ) == 0 )
        		return true;
        }
        writeLog.write(str+"\n");
        return false;
	}

	public static String createMissingFile(String line,String dir)
	{
		
        BufferedWriter writer = null;
        try
        {
        	writer = new BufferedWriter(new OutputStreamWriter(
        		new FileOutputStream(dir+"\\missingFile"+ (countMissingFile) +".txt"), "UTF8"));
            writer.close();
            if( line.split("\t").length > 2 )
              return "missingFile"+ (countMissingFile++) +".txt\t bundle:ORIGINAL\t"+line.split("\t")[2];
            else
              return "missingFile"+ (countMissingFile++) +".txt\t bundle:ORIGINAL";
        }
        catch( FileNotFoundException e ){}
		catch(IOException e){}
		return "";
	}

	public static void createContens(String dir,ArrayList<String> arrContents)
	{
		BufferedWriter writer = null;
		try
		{
			
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dir+"\\contents"), "UTF8"));
			
			for( String pdf : arrContents )
			{
				
				writer.write(pdf+"\n");

			}
			writer.close();
		}
		catch( FileNotFoundException e ){}
		catch(IOException e){}
		
		
		
	}

	public static void copyImg(String subDir)throws IOException
		{
			FileChannel inputChannel = null;

			FileChannel outputChannel = null;
            
			try {

				inputChannel = new FileInputStream(oldDir+"/"+subDir+"/"+subDir+".jpg").getChannel();
                
				outputChannel = new FileOutputStream(baseDir+"/"+subDir+"/stou"+subDir.substring(4)+".jpg").getChannel();	

				outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

			}
			catch( FileNotFoundException e )
			{
				try
				{
					inputChannel = new FileInputStream(oldDir+"/"+subDir+"/"+subDir.substring(4)+".jpg").getChannel();
                
					outputChannel = new FileOutputStream(baseDir+"/"+subDir+"/"+subDir.substring(4)+".jpg").getChannel();	

					outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
				}
				catch( FileNotFoundException fe ){}
				finally
				{
					if(inputChannel != null)
						inputChannel.close();

					if(inputChannel != null)
						outputChannel.close();
				}
				
			} 
			finally 
			{
				if(inputChannel != null)
					inputChannel.close();

				if(inputChannel != null)
					outputChannel.close();
				
			}
            

		} 
     

}