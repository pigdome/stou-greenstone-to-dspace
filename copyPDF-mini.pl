use File::Find::Rule;
use strict;
use warnings;
use File::Copy;

my $srcdir = $ARGV[0];   #pdf-mini-org
my $destdir = $ARGV[1];  #pdf-mini
my @subdir_dest = File::Find::Rule->directory->in($destdir);
my @subdir_src = File::Find::Rule->directory->in($srcdir);
	
    foreach my $subdir (@subdir_dest) 
    {
   		opendir(DIR, ( $srcdir ) ) or die $!;
		while (my $file = readdir(DIR)) 
		{
            if ( index( $file , '.pdf') >= 0 ) 
            {
            	my $subdir_temp = $subdir;
                $subdir_temp = substr( $subdir_temp , rindex( $subdir_temp , "/" )+1 , length($subdir_temp) );
                $subdir_temp .= '.pdf';
                #print "$subdir_temp :: $file";
                if( $subdir_temp eq ( "STOU" . $file  ) || uc ($subdir_temp) eq  uc ($file) )
            	{
            		my $file_resize = $file;
                    $file_resize =~ s/.pdf//g;
                    $file_resize =~ s/stou//g;
            		copy( $srcdir . '/' . $file , $subdir . '/STOU' . $file_resize . '-resize.pdf' ) or die "Copy failed: $!";
            		print "copy $file successfully \n";		
            	}
            }
		}
		closedir(DIR);
    }

