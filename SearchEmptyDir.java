import java.io.*;
import java.io.File;
import java.util.*;
import java.nio.channels.FileChannel;

public class SearchEmptyDir
{
	public static void main(String[] args)
	{
          String baseDir = "/D:/import-copy";

          for( String subdir : listDir(baseDir) ) 
          {
               /*if( ! isEmpty(listFile( baseDir+"/"+subdir )) )
                  System.out.println(subdir);  */

               //deleteMissingFile(baseDir+"/"+subdir); 
               deleteUnuseFile(baseDir+"/"+subdir);  	
          }     
	}

	public static boolean isEmpty(String[] listfile)
	{
         for (String filename : listfile ) 
         {
             if( filename.indexOf(".pdf") >=0 )
                return true;	
         }
         return false;
	}
	private static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	private static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	private static void deleteMissingFile(String subdir)
	{
		String[] listfile = listFile(subdir);

		for (String filename : listfile) 
		{
		    if( filename.indexOf("missingFile") >=0 )
		     {
		     	File file = new File(subdir+"/"+filename);
		     	file.delete();
		     }	
		}
	}
	private static void deleteUnuseFile(String subdir)
	{
		String[] listfile = listFile(subdir);

		for (String filename : listfile) 
		{
		    if( filename.indexOf("index") >=0 && filename.indexOf(".html") >=0   || 
		    	filename.indexOf("metadata") >=0 && filename.indexOf("mods") < 0 ||
		    	filename.indexOf(".html") >=0 && filename.indexOf(".bak") >=0 )
		     {
		     	File file = new File(subdir+"/"+filename);
		     	file.delete();
		     }	
		}
	}
}