import java.io.*;
import java.io.File;
import java.util.*;
import java.nio.channels.FileChannel;
import static java.nio.file.StandardCopyOption.*;
import java.nio.file.Files;
import java.nio.file.Paths;


public class CheckFile
{
	
	public static BufferedWriter writeLog = null;
	
	public static void main(String[] args)throws IOException
	{
		String  srcDir = args[0];
		String destDir = args[1];
		try
		{
			writeLog = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("logfile.txt"), "UTF8"));
		}
		catch( FileNotFoundException e ){}
		catch(IOException e){}

		String[]  srcDirList = CheckFile.listDir(srcDir);
		String[] destDirList = CheckFile.listDir(destDir);

		for( String subDir : srcDirList )
		{
			if( ! isMatch(subDir,destDirList) )
		    {
                 //MoveDir(srcDir+"/"+subDir,"/E:/stouerr");
		    	 new File("E:\\stouerr\\"+subDir).mkdir();
                 Files.move(Paths.get("D:\\import-copy\\"+subDir),Paths.get("E:\\stouerr"), REPLACE_EXISTING);
		    }
		}
		for( String subDir : destDirList )
		{
			if( ! isMatch(subDir,srcDirList) )
		    {
		    	new File("E:\\stouerr\\"+subDir).mkdir();
		    	Files.move(Paths.get("D:\\STOU-copy\\"+subDir),Paths.get("E:\\stouerr\\"+subDir), REPLACE_EXISTING);
		    }
		}
		writeLog.close();
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}
 
    private static boolean isMatch(String dir,String[] srcDirList)
    {
         for(String subDir : srcDirList)
         {
         	if( dir.compareTo(subDir)==0 )
         		return true;
         }
         return false;
    }

    private static void MoveDir(String src,String dest)
    {
    	InputStream in = null;
		OutputStream out = null;

		try {

			File oldFile = new File(src);
			File newFile = new File(dest);

			in = new FileInputStream(oldFile);
			out = new FileOutputStream(newFile);

			byte[] moveBuff = new byte[1024];

			int butesRead;

			while ((butesRead = in.read(moveBuff)) > 0) {
				out.write(moveBuff, 0, butesRead);
			}

			in.close();
			out.close();

			oldFile.delete();

			System.out.println("The File was successfully moved to the new folder");

		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	/*private static void createContents(String title,String[] listfile,String subDir)
    {
        BufferedWriter writer = null;
        try
        {
            writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/contents"), "UTF8"));
            String pdfname = "";
            for( String filename : listfile )
            {
                if(filename.indexOf(".pdf") >= 0)
                  writer.write(pdfname+"\tbundle:ORIGINAL\tdescription:"+pdfname.trim()+"\n");
            }
            
            writer.close();
        }
        catch( FileNotFoundException e ){}
        catch (IOException e) {
            e.printStackTrace();
        }
        

    }*/
    
    /*private static String getPdfName(Stirng srcDir)
    {
    	String[] listfile = CheckFile.listfile(srcDir);
    	for( String filename : listfile )
    		if( filename.indexOf(".pdf") >= 0 )
    			return filename;
    	return "";
    }*/
 

}