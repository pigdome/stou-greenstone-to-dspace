use File::Find::Rule;
use strict;
use warnings;

my $srcdir = $ARGV[0];  #pdf-mini
my @subdir_src = File::Find::Rule->directory->in($srcdir);

    foreach my $subdir (@subdir_src) 
    {
   		opendir(DIR, ( $subdir ) ) or die $!;
        my $contents = $subdir . "/contents";print $contents . "\n";
        open( OUTPUT , "> $contents" ) or die $!;
		while (my $file = readdir(DIR)) 
		{
            if ( index( $file , '.pdf') >= 0 && index( $file , 'resize') < 0 ) 
            {
                print OUTPUT $file . "\tbundle:ORIGINAL\tdescription:ต้นฉบับ\n";
            }
            elsif ( index( $file , '.pdf') >= 0  ) 
            {
                print OUTPUT $file . "\tbundle:ORIGINAL\tdescription:ไฟล์ขนาดเล็ก\n";
            }
            elsif ( index( $file , '.xml') >= 0 && index( $file , 'dublin_core') < 0 && index( $file , 'metadata_mods' ) < 0 ) 
            {
                print OUTPUT $file . "\tbundle:ORIGINAL\tdescription:MODS Original (XML)\n";
            }
            elsif ( index( $file , '.html') >= 0  ) 
            {
                print OUTPUT $file . "\tbundle:ORIGINAL\tdescription:MODS Original (HTML)\n";
            }
		}
        #closedir(OUTPUT);
		closedir(DIR);
    }
