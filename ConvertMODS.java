import java.io.*;
import java.util.*;

public class ConvertMODS
{
	public static String baseDir = "/C:/gst/collect/stou/import";

	public static void main(String[] args) 
	{
       String[] directories = ConvertMODS.listDir(baseDir);
		
		for( String subDir : directories )
		{
			String[] listfile = ConvertMODS.listFile(baseDir+"/"+subDir);
            ConvertMODS.convertMODS(subDir,listfile);
		}
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	public static void convertMODS(String subDir,String[] listfile)
	{
         String modsName = "";
         for( String str : listfile )
         	if( str.indexOf(".xml") >= 0  && str.indexOf("dublin_core") < 0 )
         		modsName = str;

         try 
         {
         	ArrayList<String> arrMODS = new ArrayList<String>();
            ArrayList<String> arrTemp = new ArrayList<String>();
			BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(baseDir+"/"+subDir+"/"+modsName), "UTF8"));
			String line;
			
			while((line=br.readLine())!= null)
			{
				if( line.indexOf("<mods:mods>") >= 0 )
			    {
			    	while( (line=br.readLine())!= null )
			    	{
			    		if( line.indexOf("<mods:") >= 0 )
			    		{
			    			String tagName = line.substring(line.indexOf("<"),line.indexOf(">")).split(":")[1];
			    		           tagName = tagName.substring(0,tagName.length());
	                        
	                        switch(tagName)
	                        {
                               case "titleInfo" : arrTemp.add( getTitle(br) );break;
                               case "titleInfo type=\"alternative\"" : arrTemp.add( getAlternative(br) );break;
                               case "name type=\"corporate\"" : arrTemp.add( getContributor(br) );break;
                               case "typeOfResource" : arrTemp.add( getType(line) );break;
                               case "genre authority=\"marc\"" : arrMODS.add( getGenre(line) );break;
                               case "placeTerm type=\"code\" authority=\"marccountry\"" : arrTemp.add( getCoverage(line,br) );break;
                               //case "placeTerm type=\"text\"" : arrTemp.add( getCoverage(line) );break;
                               case "publisher" : arrTemp.add( getPublisher(line) );break;
                               case "dateIssued encoding=\"marc\"" : arrTemp.add( getDate(line) );break;
                               case "edition" : arrMODS.add( getEdition(line) );break;
                               case "languageTerm authority=\"iso639-2b\" type=\"code\"" : arrTemp.add( getLanguage(line) );break;
                               case "extent" : arrTemp.add( getFormat(line) );break;
                               case "tableOfContents" : arrTemp.add( getDescriptionTable(line,br) );break;
                               case "note" : arrTemp.add( getDescription(line) );break;
                               case "topic" : arrTemp.add( getSubject(line) );break;
                               case "relatedItem type=\"constituent\"" : arrMODS.add( getRelateItem(br) );break;
                               case "relatedItem type=\"original\"" : arrTemp.add( getIsPartOf(br) );break;
                               case "identifier type=\"isbn\"" : arrTemp.add( getIdentifierISBN(line) );break;
                               case "identifier type=\"local\"" : arrTemp.add( getIdentifierOther(line) );break;
                               case "recordChangeDate encoding=\"iso8601\"" : arrTemp.add( getDateCreated(line) );break;
                               case "recordIdentifier source=\"VRT\"" : arrTemp.add( getIdentifierOther(line) );break;
                               case "recordIdentifier" : arrTemp.add( getIdentifierOther(line) );break;
	                        }
	                        //System.out.println(tagName);
			    		}
			    		
			    	}
			    }
			}
            
            ConvertMODS.writeToDublinCore(arrTemp,arrMODS,subDir);
		}

		catch( FileNotFoundException e ){}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

    
    public static void writeToDublinCore(ArrayList<String> arrTemp,ArrayList<String> arrMODS,String subDir)
    {
    	 ArrayList<String> arrFromOldDB = ConvertMODS.getFromDBC(subDir);
         BufferedWriter writer = null;
         try
         {
             writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(baseDir+"/"+subDir+"/dublin_core.xml"), "UTF8"));
             writer.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
			 writer.write("<dublin_core schema=\"dc\">");
			 for( String line : arrTemp )
				writer.write(line);
			 for( String line : arrFromOldDB ) 
				writer.write(line);
			 writer.write("</dublin_core>");
			 writer.close();	
         }
         catch( FileNotFoundException e ){}
		 catch (IOException e) {
			e.printStackTrace();
		 }

		 // write dublin_core.xml
         
         writer = null;
         try
         {
             writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(baseDir+"/"+subDir+"/metadata_mods.xml"), "UTF8"));
             writer.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
			 writer.write("<dublin_core schema=\"mods\">");
			 
			 for( String line : arrMODS ) 
				writer.write(line);
			 writer.write("</dublin_core>");
			 writer.close();	
         }
         catch( FileNotFoundException e ){}
		 catch (IOException e) {
			e.printStackTrace();
		 }
         

         
    }

    public static ArrayList<String> getFromDBC( String subDir )
    {
    	ArrayList<String> arr = new ArrayList<String>();
    	BufferedReader br = null;
    	String line;
    	try
    	{
    		br = new BufferedReader(new InputStreamReader(new FileInputStream(baseDir+"/"+subDir+"/dublin_core.xml"),"UTF8"));
            
            while((line=br.readLine())!= null)
            {
            	System.out.println(subDir);
                if( line.indexOf("<dcvalue element=\"relation\" qualifier=\"ispartof\">") >= 0 )
                   arr.add(line);
                if( line.indexOf("<dcvalue element=\"format\" qualifier=\"mimetype\">") >= 0 )
                   arr.add(line); 	
            }
    	}
    	catch( FileNotFoundException e ){e.printStackTrace();}
		catch (IOException e) {
			e.printStackTrace();
		}
        return arr;
    }








	
























	// Parse  section *************************************************************************************************

	public static String getTitle( BufferedReader br )throws IOException
	{
		String line;
		String out = "";
		while((line=br.readLine())!= null && line.indexOf("</mods:titleInfo>") < 0 )
		{
            out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") )+" : ";
		}
		return "<dcvalue element=\"title\" qualifier=\"none\">"+out.substring(0,out.lastIndexOf(":"))+"</dcvalue>";
	}

	public static String getAlternative( BufferedReader br )throws IOException
	{
		String line;
		String out = "";
		while((line=br.readLine())!= null && line.indexOf("</mods:titleInfo>") < 0 )
		{
			if(line.indexOf("<mods:title>") >= 0)
            out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		}
		return "<dcvalue element=\"title\" qualifier=\"alternative\">"+out+"</dcvalue>";
	}

	public static String getContributor( BufferedReader br)throws IOException
	{
		String line;
		String out = "";
		while((line=br.readLine())!= null && line.indexOf("</mods:name>") < 0 )
		{
            out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") )+". ";
		}
		out = out.substring(0,out.lastIndexOf("."));
		//out = out.substring(0,out.lastIndexOf("."));
		return "<dcvalue element=\"contributor\" qualifier=\"author\">"+out+"</dcvalue>";
	}

	public static String getType( String line )throws IOException
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"type\" qualifier=\"none\">"+out.substring(0,1).toUpperCase()+out.substring(1,out.length())+"</dcvalue>";
	}

	public static String getGenre( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue qualifier=\"none\" element=\"genre\">"+out+"</dcvalue>";
	}

	public static String getCoverage( String line,BufferedReader br)throws IOException
	{
		String out = "";
		String out2 ="";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
	    while((line=br.readLine())!= null)
		{
			if( line.indexOf("placeTerm type=\"text\"") >= 0 )
			{
				out2 += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
				break;
			}
            
		}
		if( out.indexOf("th") >= 0 )
			out = "ไทย";	
		return "<dcvalue element=\"coverage\" qualifier=\"spatial\">"+out+". "+out2+"</dcvalue>";
	}

	public static String getPublisher( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );

		return "<dcvalue element=\"publisher\" qualifier=\"none\">"+out+"</dcvalue>";
	}

	public static String getDate( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"date\" qualifier=\"issued\">"+out+"</dcvalue>";
	}

	public static String getEdition( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"edition\" qualifier=\"none\">"+out+"</dcvalue>";
	}

	public static String getLanguage( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"language\" qualifier=\"iso\">"+out+"</dcvalue>";
	}

	public static String getFormat( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"format\" qualifier=\"extent\">"+out+"</dcvalue>";
	}

    public static String getDescriptionTable( String line ,BufferedReader br)throws IOException
	{
		String out = "";
		if( line.indexOf("</mods") >= 0 )
		{
			out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		    return "<dcvalue element=\"description\" qualifier=\"tableofcontents\">"+out+"</dcvalue>";
		}
		else
		{
			out += line.substring(line.indexOf(">")+1,line.length());
			line = br.readLine();
			out += line.substring(0,line.indexOf("<"));
			return "<dcvalue element=\"description\" qualifier=\"tableofcontents\">"+out+"</dcvalue>";
		}
		
	}

	public static String getDescription( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"description\" qualifier=\"none\">"+out+"</dcvalue>";
	}

	public static String getSubject( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"subject\" qualifier=\"other\">"+out+"</dcvalue>";
	}

    public static String getRelateItem( BufferedReader br )throws IOException
	{
		String line;
		String title = "";
		String name = "";
		while((line=br.readLine())!= null && line.indexOf("</mods:relatedItem>") < 0 )
		{
			if( line.indexOf("<mods:title>") >= 0 )
              title += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
            if( line.indexOf("<mods:namePart>") >= 0 )
              name += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		}
		if( name.compareTo("") != 0 )
		 return "<dcvalue element=\"relatedItem\" qualifier=\"constituent\">"+title+" / "+name+"</dcvalue>";
        else
         return "<dcvalue element=\"relatedItem\" qualifier=\"constituent\">"+title+"</dcvalue>";
	}
    
    public static String getIsPartOf(BufferedReader br)throws IOException
    {
        String line;
		String title = "";
		String name = "";
		while((line=br.readLine())!= null && line.indexOf("</mods:relatedItem>") < 0 )
		{
			if( line.indexOf("<mods:title>") >= 0 )
              title += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
            if( line.indexOf("<mods:extent>") >= 0 )
              name += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		}
		return "<dcvalue element=\"relation\" qualifier=\"ispartofseries\">"+title+" [ "+name+" ]</dcvalue>";
    }

	public static String getIdentifierISBN( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"identifier\" qualifier=\"isbn\">"+out+"</dcvalue>";
	}

	public static String getIdentifierOther( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"identifier\" qualifier=\"other\">"+out+"</dcvalue>";
	}

	

	public static String getDateCreated( String line )
	{
		String out = "";
		out += line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );
		return "<dcvalue element=\"date\" qualifier=\"created\">"+out+"</dcvalue>";
	}




}