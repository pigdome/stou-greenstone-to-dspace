import java.io.*;
import java.util.*;
public class GenMapfile
{
	private static String baseDir = "/D:/export";
	private static String[] arrStr = new String[200];
	public static void main(String[] args) throws IOException
	{
		for (int i = 0;i < 200 ;i++ ) 
		{
			arrStr[i] = "";
		}
		genMap();
		createMap();
	}
	private static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	private static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	private static String getDir(String subDir)
	{
		String[] listfile = listFile(subDir);
        String temp = "";
		for (String filename : listfile) 
		{
		   if( filename.indexOf(".jpg") >= 0 )
		   {
              temp = filename;
              break;
		   }
		}
		temp = temp.replace("stou","");
		temp = temp.replace("STOU","");
		temp = temp.replace(".jpg","");
		temp = temp.replace(".jpg.jpg","");
		return temp;
	}

	private static String getHandle(String subDir)throws IOException
	{
		String[] listfile = listFile(subDir);
        String temp = "";
		for (String filename : listfile) 
		{
		   if( filename.indexOf("handle") >= 0 )
		   {
              temp = filename;
              break;
		   }
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(subDir+"/"+temp)));

		return br.readLine();
	}

	private static void genMap()throws IOException
	{
		for (String subDir : listDir(baseDir)) 
		{
		    int index = Integer.parseInt( subDir.substring(0,3) );
		    arrStr[index-500] = arrStr[index-500] + "STOU"+getDir(baseDir+"/"+subDir)+" "+getHandle(baseDir+"/"+subDir)+"\n";	
		}
	}
	private static void createMap()throws IOException
	{
		for (int i = 0;i < 200 ; i++) 
		{
			if( arrStr[i].compareTo("") != 0 )
			{
				try
			    {
			   	  BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("/D:/mapfile"+"/mapfile"+(i+500))));
			   	  writer.write(arrStr[i]);
			   	  writer.close();
			    }
			    catch( FileNotFoundException e ){}

			}
		   
		}
	}
}