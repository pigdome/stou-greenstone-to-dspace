import java.io.*;
import java.util.*;
import java.nio.channels.FileChannel;
public class ConvertContent
{
	public static String baseDir = "/C:/gst/collect/stou/import-copy";
	public static String newDir = "/C:/gst/collect/stou/import";
	public static boolean turn = false;
	public static void main(String[] args)
	{
		
		String[] directories = ConvertContent.listDir(baseDir);
		
		for( String subDir : directories )
		{
			String[] listfile = ConvertContent.listFile(baseDir+"/"+subDir);
            
			ConvertContent.convertMetaData(baseDir+"/"+subDir+"/metadata.xml","",subDir);
			//String no_stou = subDir.substring(4);
			turn = false;
			ConvertContent.convertHTMLtoContents("",subDir,listfile);
			//ConvertContent.convertHTMLtoContents(baseDir+"/"+subDir+"/"+no_stou+".html","",subDir,listfile);
		}
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	public static void convertMetaData(String dir,String filter,String subDir)
	{
		//String newDir =  ConvertContent.mkdir(subDir);
		ArrayList<String> arrDBC = new ArrayList<String>();

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(dir), "UTF8"));
			String line;
			String conLine;
			

			while((line=br.readLine())!= null){
				//if( sb.append(line.trim()).toString().indexOf("<Metadata") >= 0 );
				if( line.indexOf("<Metadata") >= 0 )
					arrDBC.add( ConvertContent.metsToDublinCore(line) );
			}
		}

		catch( FileNotFoundException e ){}
		catch (IOException e) {
			e.printStackTrace();
		}

		ConvertContent.createDBC(newDir+"/"+subDir,arrDBC);
	}

	public static void convertHTMLtoContents(String filter,String subDir,String[] listfile)
	{
		//String newDir =  ConvertContent.mkdir(subDir);
		ArrayList<String> arrContents = new ArrayList<String>();
		String dir = "";
		for( String html : listfile )
		{	
			if( html.indexOf(".html") >= 0 )
				if( html.indexOf(".bak") < 0 )
					dir = html;
			}
			try {

				BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(baseDir+"/"+subDir+"/"+dir), "windows-874"));
				String line;
				String contensfile="";
				String description="";
				while((line=br.readLine())!= null )
				{
					if( line.indexOf("View Full Text (PDF)") >= 0 )
					{
						while( (line=br.readLine())!= null )
						{

							if( line.indexOf("</Metadata>") >= 0 )					        
							{

								contensfile = line.substring(line.indexOf(">")+1,line.indexOf("</Metadata>"));
								while( (line=br.readLine())!= null )
								{
									if( line.indexOf("HREF=\"http://dlib.stou.ac.th/pdf/collect/stou/import/") >=0  )
									{
										description = line.substring(line.indexOf(".pdf")-25);
										description = description.substring( description.indexOf("/")+1,description.indexOf("\"") );
										break;
									}
								}
								
								arrContents.add(description+"\tbundle:ORIGINAL\tdescription:"+contensfile);
							}


						}
					}
				}
                
				

			}

			catch( FileNotFoundException e )
			{

				System.out.println(subDir);
            //ConvertContent.convertHTMLtoContentsWithSubStr(baseDir+"/"+subDir+"/"+subDir.substring(4)+".html","",subDir,listfile);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			ConvertContent.createContens(newDir+"/"+subDir,arrContents,listfile,subDir,dir);
		}



		public static String mkdir(String dir)
		{
			String newDir = "D:\\stou\\"+dir;
			File theDir = new File(newDir);

			if (!theDir.exists()) {

				try{
					theDir.mkdirs();
				} catch(SecurityException se){
					se.printStackTrace();
				}


			}
			return ""+theDir;
		}

		public static String metsToDublinCore(String line)
		{
			String name = line.substring( (line.indexOf("name=\"")+6),line.lastIndexOf("\"")  );
			String value = line.substring( line.indexOf(">")+1,line.lastIndexOf("<") );

			switch( name )
			{
				case "dc.Title" : return "<dcvalue element=\"title\" qualifier=\"none\">"+value+"</dcvalue>";
				case "dc.Language" : return "<dcvalue element=\"language\" qualifier=\"iso\">"+value+"</dcvalue>";
				case "dc.Subject" : return "<dcvalue element=\"subject\" qualifier=\"none\">"+value+"</dcvalue>";
				case "dc.Relation" : return "<dcvalue element=\"relation\" qualifier=\"ispartof\">"+value+"</dcvalue>";
				case "dc.Type" : ConvertContent.newModFile(value);break;
				case "dc.Publisher" : return "<dcvalue element=\"publisher\" qualifier=\"none\">"+value+"</dcvalue>";
				case "dc.Creator" : return "<dcvalue element=\"contributor\" qualifier=\"author\">"+value+"</dcvalue>";
				case "dc.Format" : return "<dcvalue element=\"format\" qualifier=\"mimetype\">"+value+"</dcvalue>";
				case "dc.Source" : return "<dcvalue element=\"identifier\" qualifier=\"other\">"+value+"</dcvalue>";
			}
			return "";
		}



		public static void createDBC(String dir,ArrayList<String> arrDBC)
		{
			BufferedWriter writer = null;
			try
			{

				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dir+"\\dublin_core.xml"), "UTF8"));
				writer.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>\n");
				writer.write("<dublin_core schema=\"dc\">\n");
				for( String line : arrDBC )
					writer.write(line+"\n");
				writer.write("</dublin_core>");
				writer.close();
			}
			catch( FileNotFoundException e ){}
			catch(IOException e){}



		}

		public static void createContens(String dir,ArrayList<String> arrContents,String[] listfile,String subDir,String htmlDir)
		{
			
			BufferedWriter writer = null;
			try
			{
				

				
                if( arrContents.size() == 0 ) System.out.println(subDir+"***");
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dir+"\\contents"), "UTF8"));

			//writer.write("license.txt"+"  bundle:LICENSE\n");
                String imgName = "";
				for( String line : listfile )
				{
					if( line.indexOf(".jpg") >=0 )
					{
						writer.write(line+"\tbundle:ORIGINAL\tdescription:ภาพปก\n");
						imgName = line;
					}
						
				}
                imgName = imgName.substring(0,imgName.indexOf("."));
				writer.write(imgName+".xml"+"\tbundle:ORIGINAL\tdescription:MODS Metadata\n");
                
                
				ConvertContent.copyMODS(dir,subDir,htmlDir);
			    

				for( String pdf : arrContents )
				{
				//System.out.println(arrContents.size());
					writer.write(pdf+"\n");

				}




				writer.close();
				//writeLog.close();
			}
			catch( FileNotFoundException e ){}
			catch(IOException e){}



		}

		public static void copyMODS(String dir,String subDir,String htmlDir)throws IOException
		{
			FileChannel inputChannel = null;

			FileChannel outputChannel = null;
            
			try {

				inputChannel = new FileInputStream(baseDir+"/"+subDir+"/"+htmlDir.substring(0,htmlDir.indexOf(".html"))+".xml").getChannel();
                
				outputChannel = new FileOutputStream(dir+"/"+htmlDir.substring(0,htmlDir.indexOf(".html"))+".xml").getChannel();	

				outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

			} finally {
				if(inputChannel != null)
					inputChannel.close();

				if(inputChannel != null)
					outputChannel.close();

			}


		} 
        
        

		public static void newModFile(String value)
		{

		}

	}