import java.io.*;
import java.io.File;
import java.util.*;
import java.nio.channels.FileChannel;

public class CreateContents
{
	private static String baseDir = "/D:/import-copy";
	public static void main(String[] args)
	{
          

          for( String subdir : listDir(baseDir) ) 
          {
               /*if( ! isEmpty(listFile( baseDir+"/"+subdir )) )
                  System.out.println(subdir);  */

               //deleteMissingFile(baseDir+"/"+subdir); 
               makeContents(subdir);  	
          }     
	}

	public static boolean isEmpty(String[] listfile)
	{
         for (String filename : listfile ) 
         {
             if( filename.indexOf(".pdf") >=0 )
                return true;	
         }
         return false;
	}
	private static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	private static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	private static void makeContents(String subdir)
	{
		String[] listfile = listFile(baseDir+"/"+subdir);
        BufferedWriter writer = null;
        try
        {
        	writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(baseDir+"\\"+subdir+"\\contents"), "UTF8"));
        
        
			for (String filename : listfile) 
			{
			    if( filename.indexOf(".pdf") >= 0 && filename.indexOf("resize") < 0 ) //It's pdf Original size
			    {
			    	writer.write(filename+"\tbundle:ORIGINAL\tdescription:ต้นฉบับ\n");
			    }
			}
            for (String filename : listfile) 
			{
			    if( filename.indexOf(".pdf") >= 0 && filename.indexOf("resize") >= 0 ) //It's pdf small size
			    {
			    	writer.write(filename+"\tbundle:ORIGINAL\tdescription:ไฟล์ขนาดเล็ก\n");
			    }
			}
			for (String filename : listfile) 
			{
			    if( filename.indexOf(".xml") >= 0 && filename.indexOf("dublin_core") < 0 && filename.indexOf("metadata_mods") < 0 ) //It's xml Original
			    {
			    	writer.write(filename+"\tbundle:ORIGINAL\tdescription:MODS Original (XML)\n");
			    }
			}
			for (String filename : listfile) 
			{
			    if( filename.indexOf(".html") >= 0 && filename.indexOf(subdir) < 0 ) //It's html Original
			    {
			    	writer.write(filename+"\tbundle:ORIGINAL\tdescription:MODS Original (HTML)\n");
			    }
			}    
			    
			writer.close();
		}
		catch( FileNotFoundException e ){System.out.println(e);}
		catch(IOException e){System.out.println(e);}
		//finally{ if(writer!=null)writer.close(); }
	}



}