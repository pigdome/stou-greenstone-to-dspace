import java.io.*;
import java.io.File;
import java.util.*;
import java.nio.channels.FileChannel;

public class ReplacePdf
{
	private static String srcDir = "/D:/import-copy";
    private static String destDir = "/D:/STOU";
	public static void main(String[] args)throws IOException	
	{
		
		ReplacePdf.removeAllPdfAndJpg(srcDir);
		ReplacePdf.copyPdf(destDir,srcDir);
	}

	private static void removeAllPdfAndJpg(String path)
	{
         String[] listDir = listDir(path);

         for( String subdir : listDir )
         {

         	 String[] listfile = ReplacePdf.listFile(srcDir+"/"+subdir);
             
         	 for( String filename : listfile )
         	 {
         	 	 if( filename.indexOf(".pdf") >= 0 || filename.indexOf(".jpg") >= 0)// is Pdf or Jpg file
         	 	 {
                      File file = new File(path+"/"+subdir+"/"+filename);
                      file.delete();
         	 	 }
         	 }
         }
	}

	private static void copyPdf(String srcDir,String destDir)throws IOException
	{
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
	    String[] listDir = listDir(srcDir);

         for( String subdir : listDir )
         {
         	 System.out.println("Move "+subdir+" ...");
		     try 
		     {

				inputChannel = new FileInputStream(srcDir+"/"+subdir+"/"+getPdfName(srcDir+"/"+subdir)).getChannel();
	            
				outputChannel = new FileOutputStream(destDir+"/"+subdir+"/"+getPdfName(srcDir+"/"+subdir)).getChannel();	

				outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

			}
			catch( FileNotFoundException e )
			{
				System.out.println("Move "+subdir+" ..."+e);
			} 
			finally 
			{
				if(inputChannel != null)inputChannel.close();
				if(outputChannel != null)outputChannel.close();
			}         	 
         }
	}

	private static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	private static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	private static String getPdfName(String srcDir)
    {

    	String[] listfile = ReplacePdf.listFile(srcDir);
    	System.out.println(srcDir+" "+listfile.length);
    	if( listfile != null )
    	for( String filename : listfile )
    		if( filename.indexOf(".pdf") >= 0 )
    			return filename;
    	return "";
    }
}