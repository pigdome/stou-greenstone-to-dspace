import java.io.*;
import java.util.*;
public class CheckContentFile
{
	public static String baseDir = "/C:/gst/collect/stou/import";
	//public static String newDir = "/C:/gst/collect/stou/import";
	//public static int countMissingFile = 0;
	public static void main(String[] args)
	{
		
		String[] directories = CheckContentFile.listDir(baseDir);
		
		for( String subDir : directories )
		{
			//String[] listfile = CheckFile.listFile(baseDir+"/"+subDir);

			CheckContentFile.checkFile(baseDir+"/"+subDir+"/contents",baseDir+"/"+subDir);
		}
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	/*public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}*/

	public static void checkFile(String contentsDir,String dir)
	{
		File f = new File(contentsDir);
        File folder = new File(dir);
        if( ! f.exists() )
        {
            System.out.println("no contents file in "+ dir);
        	CheckContentFile.deleteDirectory(folder);
        }
        else
        {
        	if( f.length() == 0 )
        		CheckContentFile.deleteDirectory(folder);
        }    
	}

	static public boolean deleteDirectory(File path) 
	{
	    if (path.exists()) {
	        File[] files = path.listFiles();
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory()) {
	                deleteDirectory(files[i]);
	            } else {
	                files[i].delete();
	            }
	        }
	    }
	    return (path.delete());
    }

}