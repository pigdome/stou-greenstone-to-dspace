import java.io.*;
import java.util.*;

public class ConvertHtmlToMet
{
	public static String baseDir = "/D:/gsdl/collect/nonthaburi/import-test";

	public static void main(String[] args) 
	{
       String[] directories = ConvertHtmlToMet.listDir(baseDir);
		
		for( String subDir : directories )
		{
			String[] listfile = ConvertHtmlToMet.listFile(baseDir+"/"+subDir);
            ConvertHtmlToMet.convertMODS(subDir,listfile);
		}
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}
    
    public static void convertMODS(String subDir,String[] listfile)
    {
    	BufferedWriter writer = null;
    	BufferedReader br = null;
        BufferedWriter writerMODS = null;
        BufferedWriter test = null;
        String hmtlname = "";
        String line;
        String temp="";
        String title = "";

        ArrayList<String> arrData = new ArrayList<String>();
    	for( String filename : listfile )
    		if( filename.indexOf(".html") >= 0 && filename.indexOf(".bak") < 0 )
                  hmtlname = filename;
      
    	try
    	{
            br = new BufferedReader(new InputStreamReader(
            	 new FileInputStream(baseDir+"/"+subDir+"/"+hmtlname), "windows-874"));
    	    writer = new BufferedWriter(new OutputStreamWriter(
    	    	new FileOutputStream(baseDir+"/"+subDir+"/dublin_core.xml"), "UTF8"));
    	    writer.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>\n");
    	    writer.write("<dublin_core schema=\"dc\">\n");

            writerMODS = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/metadata_mods.xml"), "UTF8"));
            writerMODS.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
            writerMODS.write("<dublin_core schema=\"mods\">");

            test = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/log.txt"), "UTF8"));
    	    


            while((line=br.readLine())!= null)
            {                     
            	if( line.indexOf("cccccc") >= 0 )
            	{
                    while((line=br.readLine())!= null)
            		{
            			if( line.indexOf("<TR") >= 0 )
            			{
            				temp = line;
            				while((line=br.readLine())!= null)
            				{
                                 if( isText(line) )
                                    temp += " "+subSpaceInStr(line);
                                 else
                                    temp += subSpaceInStr(line).trim();
                                 if( line.indexOf("</TR>") >= 0 )
                                 	break;
            				}
                            //test.write(temp);
            				arrData.add( ConvertHtmlToMet.subString(temp) );
            			}
            		}
            	}   
            }
            
            // write log file
            for( String str : arrData )
                test.write(str+"\n");

            title = ConvertHtmlToMet.choose(arrData,writer,writerMODS);
            ConvertHtmlToMet.createContents(title,listfile,subDir);

            writer.write("</dublin_core>");
            writer.close();
            writerMODS.write("</dublin_core>");
            writerMODS.close();
            test.close();
    	}
    	catch( FileNotFoundException e ){e.printStackTrace();}
		catch (IOException e) {
			e.printStackTrace();
		}
         
    }

    public static String subString(String elem)
    {
    	String line = elem;
        while( line.indexOf("<") >= 0 )
        {
            String unused = line.substring(line.indexOf("<"),line.indexOf(">")+1);
            line = line.replace(unused,"").trim();
        }
        return line;
    }

    public static String choose(ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
         String title = "";
         int i = 0;
         for( String line : arrData )
         {
              switch( line.trim() )
              {
                 case "TitleInformation"        : title = getInfoInTitle(i,arrData,writer,writerMODS);break;
                 case "Language"                : getInfoInLanguage(i,arrData,writer,writerMODS);break;
                 case "Subject"                 : getInfoInSubject(i,arrData,writer,writerMODS);break;
                 case "Subject(Authority)"      : getInfoInSubject(i,arrData,writer,writerMODS);break;
                 case "Related Item :Host (In)" : getInfoInRHost(i,arrData,writer,writerMODS);break;
                 case "Related Item :series"    : getInfoInRSeries(i,arrData,writer,writerMODS);break;
                 case "PhysicalDescription"     : getInfoInPhysical(i,arrData,writer,writerMODS);break;
              }
              i++;
         }
         return title;
    }
    
    private static String getInfoInTitle(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String namepart = "";
        String title = "";
        String displayfForm = "";
        String affiliation = "";
        String line = arrData.get(j);
        while( line.indexOf("Language") < 0 && j < arrData.size())
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("TitleInformation") < 0 && line.split(":").length > 1 && j < 5)
            {
              title =  line.split(":")[1].trim();
              writer.write("<dcvalue element=\"title\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");
            }

            if( line.indexOf("Name Part : given :") >= 0 )
              namepart = line.split(":")[2].trim();

            if( line.indexOf("Name Part") >= 0 && line.indexOf("given") < 0)
              namepart = line.split(":")[1].trim();

            if( line.indexOf("Role Term : Text") >= 0 && line.split(":").length > 2)
              writer.write("<dcvalue element=\"contributor\" qualifier=\"author\">"+namepart+", "+line.split(":")[2].trim()+"</dcvalue>\n");
        
            if( line.indexOf("Place Term : Text :") >= 0 && line.split(":").length > 2)
              writerMODS.write("<dcvalue element=\"originInfo\" qualifier=\"place\">"+line.split(":")[2].trim()+"</dcvalue>\n");

            if( line.indexOf("Type ofResource") >= 0 )
              writer.write("<dcvalue element=\"type\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Date Issued (marc)") >= 0 && j < 20)
              writer.write("<dcvalue element=\"date\" qualifier=\"issued\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Genre") >= 0 )
              writerMODS.write("<dcvalue element=\"genre\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("displayfForm :") >= 0 )
              displayfForm = line.split(":")[1].trim();
            
            if( line.indexOf("affiliation :") >= 0 )
              affiliation = line.split(":")[1].trim();
            // next cursor          
            line = arrData.get(j++);
        }
        if( displayfForm.compareTo("") != 0 && affiliation.compareTo("") != 0 )
        writer.write("<dcvalue element=\"description\" qualifier=\"none\">"+displayfForm+", "+affiliation+"</dcvalue>\n");

        return title;
    }

    private static void getInfoInLanguage(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("PhysicalDescription") < 0 && j < 20)
        {                    
            if( line.indexOf("Language") >= 0 && line.indexOf("Term") >= 0 )
            {
              if(line.split(":")[2].trim().compareTo("Thai") == 0 || line.split(":")[2].trim().compareTo("thai") == 0)
                 writer.write("<dcvalue element=\"language\" qualifier=\"iso\">tha</dcvalue>\n");
              else
                writer.write("<dcvalue element=\"language\" qualifier=\"iso\">"+line.split(":")[2].trim()+"</dcvalue>\n");
            }

            // next cursor          
            line = arrData.get(j++);
        }
    }

    private static void getInfoInPhysical(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("Subject") < 0 && j < arrData.size())
        {
            if( line.indexOf("Extent") >= 0 && line.indexOf("Pages")<0)
              writer.write("<dcvalue element=\"format\" qualifier=\"extent\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("digitalOrigin") >= 0 )
              writerMODS.write("<dcvalue element=\"physicalDescription\" qualifier=\"digitalOrigin\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Table ofContents (contents)") >= 0 )
              writer.write("<dcvalue element=\"description\" qualifier=\"tableofcontents\">"+line.split(":")[1].trim()+"</dcvalue>\n");

            if( line.indexOf("Note") >= 0 )
              if( line.split(":").length == 3 )
               writer.write("<dcvalue element=\"description\" qualifier=\"none\">"+line.split(":")[2].trim()+"</dcvalue>\n");
              else
               writer.write("<dcvalue element=\"description\" qualifier=\"none\">"+line.split(":")[1].trim()+"</dcvalue>\n");
             
            // next cursor Note:         
            line = arrData.get(j++);
        }
    }

    private static void getInfoInSubject(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        int j = i;
        String subject= "";
        String line = arrData.get(1+j);
        
        int k = i+1;
        while( true )
        {
              if( k == arrData.size()-1 )
                break;
              if( arrData.get(k).indexOf("Subject") >= 0 )
                break;
              if( arrData.get(k).indexOf("Related") >= 0 )
                break;

              k++;    
        }
        
        while( j < k )
        {
            line = arrData.get(j);
            if( line.indexOf("Geographic") >= 0 && line.split(":").length > 1)
            {
              subject += line.split(":")[1].trim()+"--";
            }
            
            if( line.indexOf("Name Part") >= 0 )
            {
              if( line.split(":").length == 2 )
                subject += line.split(":")[1].trim()+"--";
              else if( line.split(":").length == 3 )
                subject += line.split(":")[2].trim()+"--";
            }

            if( line.indexOf("Topic") >= 0 && line.split(":").length > 1)
            {
              subject += line.split(":")[1].trim()+"--";
            }
             
            // next cursor 
            if( j < arrData.size() )         
            line = arrData.get(j++);
            else break;
        }
        if( subject.indexOf("--") >= 0 )
        subject =  subject.substring(0,subject.lastIndexOf("--"));
        writer.write("<dcvalue element=\"subject\" qualifier=\"none\">"+subject+"</dcvalue>\n");
    }                          

    
    private static void getInfoInRHost(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        String title = "";
        ArrayList<String> partname = new ArrayList<String>();
        ArrayList<String> affiliation = new ArrayList<String>();
        ArrayList<String> role = new ArrayList<String>();
        String genre = "";
        String start = "";
        String end = "";
        String note = "";
        String form = "";
        String lcc = "";
        String isbn = "";
        String issuance="";
        String volume = "";
        String issue = "";
        String year = "";
    
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("Related Item :series") < 0 && j < arrData.size())
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("Title Information") < 0)
              title = line.split(":")[1].replace(":","").trim();

            /*if( line.indexOf("partName") >= 0 )
              partname = line.split(":")[1].trim();*/

            if( line.indexOf("Name Part : given") >= 0 )
              partname.add(line.split(":")[2].trim());

            if( line.indexOf("Genre") >= 0 )
              genre = line.split(":")[1].trim();

            if( line.indexOf("Start") >= 0 )
              start = line.replace("Start","").replace(":","").trim();

            if( line.indexOf("End") >= 0 )
              end = line.replace("End","").replace(":","").trim();
            
            if( line.indexOf("affiliation") >= 0 )
              affiliation.add(line.split(":")[1].trim());

            if( line.indexOf("Identifier : ISBN") >= 0 )
              isbn = line.split(":")[2].trim();
            // Issuance
            
            if( line.indexOf("Part Detail : volume") >= 0 )
              volume = arrData.get(j).replace("Part Number","").trim(); 
            
            if( line.indexOf("Part Detail : issue") >= 0 )
              issue = arrData.get(j).replace("Part Number","").trim();

            if( line.indexOf("Date (marc)") >= 0 )
              year = line.split(":")[1].trim(); 

            if( line.indexOf("Issuance") >= 0 )
              issuance = line.split(":")[1].trim();

            if( line.indexOf("Classification (LCC)") >= 0 )
              lcc = line.split(":")[1].trim();

            if( line.indexOf("Form (marcform)") >= 0 )
              form = line.split(":")[1].trim();

            if( line.indexOf("Role Term : Text") >= 0 )
              role.add(line.replace("Role Term : Text","").trim());              
            
            if( line.indexOf("Note") >= 0 )
              if(line.split(":").length == 2)
                note = line.split(":")[1].trim(); 
              else if( line.split(":").length == 3 )
                note = line.split(":")[2].trim();
             
            // next cursor          
            line = arrData.get(j++);
        }
        String personal = "";
        int k = 0;
        for(String name : partname)
        {
            if(affiliation.size() > k && affiliation.size() > 0)
             personal = personal + name+", "+role.get(k)+" ("+affiliation.get(k)+");";
            else if(role.size() > k && role.size() > 0)
             personal = personal + name+", "+role.get(k)+";";
            else
             personal = personal + name+";";
            k++; 
        }
        if( personal.indexOf(";") >= 0 )
        personal = personal.substring(0,personal.lastIndexOf(";"));

        String output = "";
        
        if(note.compareTo("") != 0)
          output = form +" : "+ title +" ["+genre+ "] / " + personal +" , "+start+"-"+end+" ["+note+"]";
        else if( start.compareTo("") != 0 )
          output = form +" : "+ title +" ["+genre+ "] / " + personal +" , "+start+"-"+end;
        else
          output = form +" : "+ title +" ["+genre+ "] / " + personal;

        if( isbn.compareTo("") != 0 && lcc.compareTo("") != 0 )
          output = title +"( ISBN : "+isbn + "| "+lcc+" )";

        if( issuance.compareTo("continuing") == 0 )
            if( personal.compareTo("") != 0 )
               output = title+" / "+personal+" "+volume+", "+issue+" "+year+" : "+start+"-"+end;
            else
               output = title+". "+volume+", "+issue+" "+year+" : "+start+"-"+end;

        writer.write("<dcvalue element=\"relation\" qualifier=\"ispartof\">"+output+"</dcvalue>\n");

    }

    private static void getInfoInRSeries(int i,ArrayList<String> arrData,BufferedWriter writer,BufferedWriter writerMODS)throws IOException
    {
        String title= "";
        String partname = "";
        int j = i;
        String line = arrData.get(j);
        while( line.indexOf("RecordInformation") < 0 )
        {
            if( line.indexOf("Title") >= 0 && line.indexOf("Title Information") < 0)
              title = line.split(":")[1].trim();

            if( line.indexOf("partmane") >= 0 || line.indexOf("partname") >= 0)
              partname = line.split(":")[1].trim();
            
            // next cursor          
            line = arrData.get(j++);
        }
        if(title.indexOf(":") >= 0)
            title.substring(0,title.lastIndexOf(":"));
        if( partname.compareTo("") != 0 )
          writer.write("<dcvalue element=\"relation\" qualifier=\"ispartofseries\">"+title+" : "+partname+"</dcvalue>\n");
        else
          writer.write("<dcvalue element=\"relation\" qualifier=\"ispartofseries\">"+title+"</dcvalue>\n");
    }                          
                  

    private static String subSpaceInStr(String str)
    {
         String[] split = str.split(" ");
         String out = "";
         for( String word : split )
         	if( word.compareTo("") != 0 )
         		out += word+" ";
         return out.replace("\n","");
    }

    private static void createContents(String title,String[] listfile,String subDir)
    {
        BufferedWriter writer = null;
        try
        {
            writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(baseDir+"/"+subDir+"/contents"), "UTF8"));
            String pdfname = "";
            String htmlname ="";
            for( String filename : listfile )
            {
                if(filename.indexOf(".pdf") >= 0)
                    pdfname = filename;
                if( filename.indexOf(".html") >= 0 && filename.indexOf(".bak") < 0 )
                    htmlname = filename;
            }
            if( pdfname.compareTo("") == 0 )
            {
                // img dir
                ArrayList<String> imgname = new ArrayList<String>();
                for( String filename : listfile )
                {
                    if(filename.indexOf(".jpg") >= 0 && filename.indexOf("_A") >= 0)
                        imgname.add(filename);
                }
                for( String img : imgname )
                {
                    writer.write(img+"\tbundle:ORIGINAL\tdescription:"+title.trim()+"\n");
                }
            }
            else
            {
                // pdf dir
                writer.write(pdfname+"\tbundle:ORIGINAL\tdescription:"+title.trim()+"\n");
            }
            if( htmlname.compareTo("") != 0 )
                writer.write(htmlname+"\tbundle:ORIGINAL\tdescription:source metadata\n");

            writer.close();
        }
        catch( FileNotFoundException e ){}
        catch (IOException e) {
            e.printStackTrace();
        }
        

    }

    private static boolean isText(String line)
    {
        for( int i = 0 ; i < line.length() ; i++ )
            if( (int)line.charAt(i) >= 3584 && (int)line.charAt(i) <= 3664 && line.indexOf("</TD>") >= 0)
               return true;
        return false;
    }
	

}