import java.io.*;
import java.io.File;
import java.util.*;
import java.nio.channels.FileChannel;

public class RemoveOrgPDF
{
	public static void main(String[] args)
	{
          String baseDir = "/D:/import-copy";

          for( String subdir : listDir(baseDir) ) 
          {
               /*if( ! isEmpty(listFile( baseDir+"/"+subdir )) )
                  System.out.println(subdir);  */

               //deleteMissingFile(baseDir+"/"+subdir); 
               deleteOrgPDFFile(baseDir+"/"+subdir);  	
          }     
	}

	
	private static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	private static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	private static void deleteOrgPDFFile(String subdir)
	{
		String[] listfile = listFile(subdir);

		for (String filename : listfile) 
		{
		    if( filename.indexOf(".pdf") >=0 && filename.indexOf("resize") < 0)
		     {
		     	File file = new File(subdir+"/"+filename);
		     	file.delete();
		     }	
		}
	}
}