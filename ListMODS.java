import java.io.*;
import java.util.*;
import java.nio.channels.FileChannel;
public class ListMODS
{
	public static String baseDir = "/C:/gst/collect/stou/import";
	//public static String oldDir = "/C:/gst/collect/stou/import-copy";
	
	public static void main(String[] args)throws IOException
	{
		
		String[] directories = CheckFile.listDir(baseDir);
		
		for( String subDir : directories )
		{
			String[] listfile = CheckFile.listFile(baseDir+"/"+subDir);
			ListMODS.checkFile(listfile,subDir);
		}
	}

	public static String[] listDir(String path)
	{
		File file = new File(path);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	public static String[] listFile(String path)
	{
		File file = new File(path);
		String[] listfile = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) 
			{
				return new File(current, name).isFile();
			}
		});

		return listfile;
	}

	public static void checkFile(String[] listfile,String subDir)throws IOException
	{
		String filename = "";
		for( String str : listfile )
		{
			if( str.indexOf(".jpg") >= 0 )
				filename = str;
		}
		if( filename.indexOf("STOU") >= 0 )
		{
			FileChannel inputChannel = null;

			FileChannel outputChannel = null;
            
			try {

				inputChannel = new FileInputStream(baseDir+"/"+subDir+"/"+filename).getChannel();
                
				outputChannel = new FileOutputStream(baseDir+"/"+subDir+"/stou"+filename.substring(4)).getChannel();	

				outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

			}
			catch( FileNotFoundException e ){}
			catch(IOException e){}
			finally 
			{
				if(inputChannel != null)
					inputChannel.close();

				if(inputChannel != null)
					outputChannel.close();
				
			}
			//File old = new File(baseDir+"/"+subDir+"/"+filename);
			//old.delete();
		}
		

		
	}

	

	

}